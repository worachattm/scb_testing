package com.scb.testing.swaggerexample.controller;

import com.scb.testing.swaggerexample.model.UserDto;
import com.scb.testing.swaggerexample.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
//@RequestMapping("/user")
@Api(value = "Section User", description = "Shows the user info")
public class UserController {

	
	List<UserDto> users = new ArrayList<UserDto>();
	{
		users.add(new UserDto("Sajal", "IV"));
		users.add(new UserDto("Lokesh", "V"));
		users.add(new UserDto("Kajal", "III"));
		users.add(new UserDto("Sukesh", "VI"));
	}
	
	@ApiOperation(value = "List user")
    @RequestMapping(value="/users", method = RequestMethod.GET)
    public ResponseEntity<Object> users() {
    	
		UserService service = new UserService();
	    return new ResponseEntity<Object>(service.getUsers(), HttpStatus.OK);
    	//return new ResponseEntity<UserDto>(users, HttpStatus.OK);
    }
    
	@ApiOperation(value = "Delete user")
    @RequestMapping(value="/users/delete", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteUsersLogin() {

      	try {
      		//fileService.removeFile(id);
	    	return ResponseEntity.status(HttpStatus.OK).body("Success!");
	    } catch (Exception e) {
	    	return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
	    }	

    }
    
	@ApiOperation(value = "Insert user")
    @RequestMapping(value="/users/insertPerson", method = RequestMethod.POST)
    public ResponseEntity<String> insertPerson(@RequestBody UserDto userDto) {

      	try {
      		//fileService.removeFile(id);
	    	return ResponseEntity.status(HttpStatus.OK).body("Success!");
	    } catch (Exception e) {
	    	return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
	    }
    }
    
	@ApiOperation(value = "Insert order book")
    @RequestMapping(value="/users/orders", method = RequestMethod.POST)
    public ResponseEntity<String> OrderBook(@RequestParam(value = "order[]") int[] orders) {

      	try {
      		//fileService.removeFile(id);
	    	return ResponseEntity.status(HttpStatus.OK).body("Success!");
	    } catch (Exception e) {
	    	return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
	    }
    }
}
