package com.scb.testing.swaggerexample.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.scb.testing.swaggerexample.service.UserService;


@RestController
//@RequestMapping("/login")
@Api(value = "Section Login", description = "Authenticate")
public class LoginController {

	@Autowired
	private UserService userService;
	  
    @ApiOperation(value = "authenticate token")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "Successful"),
                    
            }
    )
    
    @RequestMapping(value="/login", method = RequestMethod.POST)
    public ResponseEntity<String> login(String username , String password) {
    	String result = userService.signin(username, password);
    	return ResponseEntity.status(HttpStatus.OK).body(result);
    	//return ResponseEntity.status(HttpStatus.OK).body("");
    }
    

}

