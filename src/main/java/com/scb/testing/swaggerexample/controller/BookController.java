package com.scb.testing.swaggerexample.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.scb.testing.swaggerexample.model.BookDto;
import com.scb.testing.swaggerexample.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
//@RequestMapping("/book")
@Api(value = "Section Book", description = "Shows the book info")
public class BookController {
	
//	List<BookDto> books = new ArrayList<BookDto>();
//	{
//		books.add(new BookDto("BookOne", "IV" , 1));
//		books.add(new BookDto("BookTwo", "V" , 2));
//		books.add(new BookDto("BookThree", "III" , 3));
//		books.add(new BookDto("BookFour", "VI" , 4));
//	}

//    @ApiResponses(
//            value = {
//                    //@ApiResponse(code = 100, message = "100 is the message"),
//                    @ApiResponse(code = 200, message = "Successful"),
//                    
//            }
//    )
//    
	@ApiOperation(value = "List books")
    @RequestMapping(value="/books", method = RequestMethod.GET)
//    @ApiImplicitParams({
//        @ApiImplicitParam(name = "Authorization", value = "Authorization token", 
//                          required = true, dataType = "string", paramType = "header") })
    public ResponseEntity<Object> books() {
		
		UserService service = new UserService();
	    return new ResponseEntity<Object>(service.getBooks(), HttpStatus.OK);
    }
    

}

