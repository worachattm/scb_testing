package com.scb.testing.swaggerexample.security;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.security.config.core.GrantedAuthorityDefaults;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.scb.testing.swaggerexample.model.USER;
//import com.scb.testing.swaggerexample.repository.UserRepository;
import com.scb.testing.swaggerexample.model.UserDto;
import com.scb.testing.swaggerexample.service.UserService;
@Service
public class MyUserDetails implements UserDetailsService {

  //@Autowired
  //private UserRepository userRepository;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
	  
//		List<UserDto> users = new ArrayList<UserDto>();
//		{
//			users.add(new UserDto("Sajal", "$2a$12$zGUd1BTlHgI/sLjoCN5nquoNUirbHH0TDyQOowg5nSR.W88NJ7LIy"));
//			users.add(new UserDto("Lokesh", "V"));
//			users.add(new UserDto("Kajal", "III"));
//			users.add(new UserDto("Sukesh", "VI"));
//		}
//		
//		UserDto userDto = users.stream().filter(c -> c.getUsername().equals(username)).findFirst().orElse(null);
//		
		
	UserService service = new UserService();
    List<UserDto> userList = service.getUsers();
    
    UserDto userDto = userList.stream().filter(c -> c.getUsername().equals(username)).findFirst().orElse(null);
    
    //ModelMapper modelMapper = new ModelMapper();
    //UserDto userDto = modelMapper.map(userModel, UserDto.class);
    
	// Create your mapper
    //ModelMapper modelMapper = new ModelMapper();

    // Create a TypeMap for your mapping
//    TypeMap<USER, UserDto> typeMap = 
//        modelMapper.createTypeMap(USER.class, UserDto.class);

    // Define the mappings on the type map
//    typeMap.map(userModel, userDto);
    
	
    if (userDto == null) {
      throw new UsernameNotFoundException("User '" + username + "' not found");
    }

    return org.springframework.security.core.userdetails.User
        .withUsername(username)//
        .password(userDto.getPassword())//
        .authorities(userDto.getAuthorities())//
        .accountExpired(false)//
        .accountLocked(false)//
        .credentialsExpired(false)//
        .disabled(false)//
        .build();
  }

}
