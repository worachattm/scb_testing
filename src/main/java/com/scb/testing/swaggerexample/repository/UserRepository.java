//package com.scb.testing.swaggerexample.repository;
//
//import javax.transaction.Transactional;
//import org.springframework.data.jpa.repository.JpaRepository;
//
//import com.scb.testing.swaggerexample.model.UserDto;
//
//public interface UserRepository extends JpaRepository<UserDto, Integer> {
//
//  boolean existsByUsername(String username);
//
//  UserDto findByUsername(String username);
//
//  @Transactional
//  void deleteByUsername(String username);
//
//}
