package com.scb.testing.swaggerexample.service;


import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.scb.testing.swaggerexample.exception.CustomException;
import com.scb.testing.swaggerexample.model.BookDto;
import com.scb.testing.swaggerexample.model.USER;
import com.scb.testing.swaggerexample.model.UserDto;
//import com.scb.testing.swaggerexample.repository.UserRepository;
import com.scb.testing.swaggerexample.security.JwtTokenProvider;

//import org.hibernate.Session;
//import org.hibernate.SessionFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

@Service
public class UserService {

//  static Session sessionObj;
//  static SessionFactory sessionFactoryObj;
  final static String db_connect_string = "jdbc:sqlserver://103.76.180.38:1433;databaseName=TKPI";
  final static String db_userid = "sa2";
  final static String db_password = "qwer1234";
  //@Autowired
  //private UserRepository userRepository;

  //@Autowired
  //private PasswordEncoder passwordEncoder;

  @Autowired
  private JwtTokenProvider jwtTokenProvider;

  @Autowired
  private AuthenticationManager authenticationManager;

  public String signin(String username, String password) {
    try {
      //password = passwordEncoder.encode("thisismysecret");
      authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
      return jwtTokenProvider.createToken(username);
    } catch (AuthenticationException e) {
      throw new CustomException("Invalid username/password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
    }
  }
  
//  public List<USER> getUsers() {
//      //Session session = getSessionFactory().openSession();
//      sessionObj = buildSessionFactory().openSession();
//
//      @SuppressWarnings("unchecked")
//      List<USER> usersModel = new ArrayList<USER>();
//      USER = sessionObj.createQuery("FROM USER").list();
//      sessionObj.close();
//      System.out.println("Found " + usersModel.size() + " usersModel");
//      return usersModel;
//  }
//
//  private static SessionFactory buildSessionFactory() {
//      // Creating Configuration Instance & Passing Hibernate Configuration File
//      Configuration configObj = new Configuration();
//      configObj.configure("hibernate.cfg.xml");
//
//      // Since Hibernate Version 4.x, ServiceRegistry Is Being Used
//      ServiceRegistry serviceRegistryObj = new StandardServiceRegistryBuilder().applySettings(configObj.getProperties()).build();
//
//      // Creating Hibernate SessionFactory Instance
//      sessionFactoryObj = configObj.buildSessionFactory(serviceRegistryObj);
//      return sessionFactoryObj;
//  }

  
  private Connection dbConnect() {
      Connection con = null;
      try {
          Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
          con = DriverManager.getConnection(""
                  + db_connect_string + ";" + "user=" + db_userid + ";" + "password=" + db_password + ";");
          if (con != null) {
              System.out.println("Database Connected.");
          } else {
              System.out.println("Database Connect Failed.");
          }

      } catch (Exception e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
      }
      return con;
  }
  
  public List<UserDto> getUsers() {
      Connection con = null;
      Statement s = null;
      List<UserDto> list = new ArrayList<UserDto>();
      
      try {
          con = dbConnect();
          s = con.createStatement();
          String sql = "SELECT * FROM USER_TESTING";
          ResultSet rec = s.executeQuery(sql);
          while((rec!=null) && (rec.next())){
              UserDto user = new UserDto();
              user.setId(rec.getInt("ID"));
              user.setUsername(rec.getString("USERNAME"));
              user.setPassword(rec.getString("PASSWORD"));
              user.setName(rec.getString("NAME"));
              user.setSurname(rec.getString("SURNAME"));
              user.setDateOfBirth(rec.getString("DATE_OF_BIRTH"));
              list.add(user);
          }
      }catch(Exception e){
          e.printStackTrace();
      }
      return list;
  }
  
  public List<BookDto> getBooks() {
      Connection con = null;
      Statement s = null;
      List<BookDto> list = new ArrayList<BookDto>();
      
      try {
          con = dbConnect();
          s = con.createStatement();
          String sql = "SELECT * FROM BOOK_TESTING";
          ResultSet rec = s.executeQuery(sql);
          while((rec!=null) && (rec.next())){
              BookDto book = new BookDto();
              book.setId(rec.getInt("ID"));
              book.setName(rec.getString("BOOK_NAME"));
              book.setAuthor(rec.getString("BOOK_AUTHOR")); 
              book.setPrice(rec.getFloat("BOOK_PRICE"));
              book.setIs_recommended(rec.getBoolean("IS_RECOMMENDED"));
              list.add(book);
          }
      }catch(Exception e){
          e.printStackTrace();
      }
      return list;
  }

}
