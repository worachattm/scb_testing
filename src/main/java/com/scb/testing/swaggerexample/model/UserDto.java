package com.scb.testing.swaggerexample.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import io.swagger.annotations.ApiModelProperty;

public class UserDto {
	
	public UserDto() {
		this.rolename = "ROLE_CLIENT";
	}
	
    public UserDto(String username, String password) {
        this.username = username;
        this.password = password;
        this.rolename = "ROLE_CLIENT";
    }

    public List<GrantedAuthority> getAuthorities() {

        List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
        grantedAuthorities.add(new SimpleGrantedAuthority(this.rolename));
        
        return grantedAuthorities;
    }

    @ApiModelProperty(notes = "username of the User")
    private String username;
    
    @ApiModelProperty(notes = "password of the User")
    private String password;

    @ApiModelProperty(notes = "surname of the user")
    private String surname;
    
    @ApiModelProperty(notes = "name of the user")
    private String name;
    
    private int Id;
    
    public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private String rolename;
   
    public String getRolename() {
		return rolename;
	}

	public void setRolename(String rolename) {
		this.rolename = rolename;
	}
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	@ApiModelProperty(notes = "date of birth of the user")
    private String dateOfBirth;

}