package com.scb.testing.swaggerexample.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "USER")
public class USER implements Serializable{
    @Id
    @Column(name = "ID")
    private Integer id;
    
    @Column(name = "USERNAME")
    private String username;
    
    
    @Column(name = "PASSWORD")
    private String password;
    
    
    @Column(name = "DATE_OF_BIRTH")
    private Date dateOfBirth;
    
	@Column(name = "SURNAME")
    private String surname;
    
    @Column(name = "NAME")
    private String name;
    
    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


}
