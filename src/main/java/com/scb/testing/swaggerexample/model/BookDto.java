package com.scb.testing.swaggerexample.model;

import io.swagger.annotations.ApiModelProperty;

public class BookDto {
	
	public BookDto() {
		
	}
	
    public BookDto(String name, String author , float price) {
        this.name = name;
        this.author = author;
        this.price = price;
    }
    
    private int id;

    public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ApiModelProperty(notes = "name of the Book")
    private String name;
    
    @ApiModelProperty(notes = "author of the Book")
    private String author;

    @ApiModelProperty(notes = "price of the Book")
    private float price;
    
    @ApiModelProperty(notes = "is recommended of the Book")
    private boolean is_recommended;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public boolean isIs_recommended() {
		return is_recommended;
	}

	public void setIs_recommended(boolean is_recommended) {
		this.is_recommended = is_recommended;
	}
}

